# Healthz (extra) Checks

Provides extra checks for [healthz](dgo.to/healthz) module.

Currently implemented:

- elasticsearch_connector module (with search_api)
